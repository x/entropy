# Copyright (C) 2013 Yahoo! Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import abc

import six


@six.add_metaclass(abc.ABCMeta)
class Backend(object):
    """Base class for persistence backends."""

    def __init__(self, conf):
        if not conf:
            conf = {}
        if not isinstance(conf, dict):
            raise TypeError("Configuration dictionary expected not: %s"
                            % type(conf))
        self._conf = conf
        self._audit = 'audit'
        self._repair = 'repair'

    @abc.abstractmethod
    def open(self):
        pass

    @abc.abstractmethod
    def close(self):
        """Closes any resources this backend has open."""
        pass

    @abc.abstractmethod
    def get_audits(self):
        pass

    @abc.abstractmethod
    def get_repairs(self):
        pass

    @abc.abstractmethod
    def audit_cfg_from_name(self, name):
        pass

    @abc.abstractmethod
    def repair_cfg_from_name(self, name):
        pass

    @abc.abstractmethod
    def get_script_cfg(self, script_type):
        pass

    @abc.abstractmethod
    def check_script_exists(self, script_type, script_name):
        pass

    @abc.abstractmethod
    def add_script(self, script_type, data):
        pass

    @abc.abstractmethod
    def remove_script(self, script_type, script_name):
        pass
